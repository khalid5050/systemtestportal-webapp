/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"github.com/go-xorm/xorm"
	"github.com/pkg/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type LabelsSQL struct {
	engine *xorm.Engine
}

// PROJECT ------------------------------------------------------------------------------------------------------------|
func(labelsSql LabelsSQL) GetLabelsForProject(testProject *project.Project)([]*project.Label, error) {
	var labels = make([] * project.Label, 0 )

	// Find the labels associated to the project
	err := labelsSql.engine.Where("project_id = ?", testProject.Id).Find(&labels)
	if err != nil {
		return nil, err
	}

	return labels, nil
}

/*
Permanently deletes a label
 */
func(labelsSql LabelsSQL) DeleteLabel(labelId int64) error {
	// Delete it from the scope
	_, err := labelsSql.engine.ID(labelId).Delete(&project.Label{})
	if err != nil {
		return err
	}

	// Make sure its gone for good
	_, err = labelsSql.engine.Id(labelId).Unscoped().Delete(&project.Label{})
	if err != nil {
		return err
	}

	// Delete the Associated TestLabels
	_, err = labelsSql.engine.Delete(&project.TestLabel{LabelId: labelId,})
	if err != nil {
		return err
	}

	// Make sure they are gone for good
	_, err = labelsSql.engine.Unscoped().Delete(&project.TestLabel{LabelId: labelId,})
	if err != nil {
		return err
	}

	return nil
}

func(labelsSql LabelsSQL) UpdateLabelForProject(label *project.Label, projectId int64) (int64, error) {

	// Is this an insert ?
	if(label.Id == -1){
		// Create a new label
		var newLabel = project.Label{
			ProjectId: projectId,
			Name: label.Name,
			Description: label.Description,
			Color: label.Color,
			TextColor: label.TextColor,
		}

		// Insert it
		_, err := labelsSql.engine.Insert(&newLabel)
		if err != nil {
			return -1, err
		}

		// Return its id
		return newLabel.Id, nil
	} else {
		_, err := labelsSql.engine.Id(label.Id).Update(label)
		if err != nil {
			return -1, err
		}

		return -1, nil
	}
}

// TEST ---------------------------------------------------------------------------------------------------------------|

func(labelsSql LabelsSQL) InsertTestLabel(labelId int64, testId int64, projectId int64, isCase bool) error {
	// Check if has
	var testLabelCheck = project.TestLabel{}

	// Query the testlabels table
	has, err := labelsSql.engine.UseBool().
		Where("label_id = ?", labelId).
		And("test_id = ?", testId).
		And("project_id = ?", projectId).
		And("is_case = ?", isCase).Get(&testLabelCheck)
	if err != nil {
		return err
	}

	if has {
		return errors.New("The requested label is already assigned")
	}

	// Insert
	var testLabel = &project.TestLabel{
		LabelId: labelId,
		TestId: testId,
		ProjectId: projectId,
		IsCase: isCase,
	}

	_, err = labelsSql.engine.UseBool().Insert(testLabel)
	if err != nil {
		return err
	}

	return nil
}




func(labelsSql LabelsSQL) GetLabelsForTest(testObject interface{}, projectId int64) ([]*project.Label, error){
	var testId int64
	var isCase bool
	var testLabels []*project.TestLabel
	var labels = make([] * project.Label, 0)

	// Build the testID based on if its a case or a sequence or a testId object
	if testCase, ok := testObject.(test.Case); ok {
		testId = testCase.Id
		isCase = true
	} else if testSequence, ok := testObject.(test.Sequence); ok {
		testId = testSequence.Id
		isCase = false
	} else {
		return nil, errors.New("Error while casting test to case or sequence or testId for Id generation")
	}

	// Query the testlabels table
	err := labelsSql.engine.UseBool().
		Where("test_id = ?", testId).
		And("project_id = ?", projectId).
		And("is_case = ?", isCase).Find(&testLabels)
	if err != nil {
		return nil, err
	}

	// Finally get the Labels themselves
	for _, testLabel := range testLabels {
		var label = project.Label{Id: testLabel.LabelId}

		has, err := labelsSql.engine.Get(&label)

		if err != nil {
			return nil, err
		} else if has {
			labels = append(labels, &label)
		}
	}

	return labels, nil

}

/*
Permanently deletes a test label
 */
func(labelsSql LabelsSQL) DeleteTestLabel(labelId int64, testId int64, projectId int64, isCase bool) error {
	// Delete it from the scope
	_, err := labelsSql.engine.UseBool().Delete(&project.TestLabel{
		LabelId: labelId,
		TestId: testId,
		ProjectId: projectId,
		IsCase: isCase,})

	if err != nil {
		return err
	}

	// Make sure its gone for good
	_, err = labelsSql.engine.UseBool().Unscoped().Delete(&project.TestLabel{
		LabelId: labelId,
		TestId: testId,
		ProjectId: projectId,
		IsCase: isCase,})

	if err != nil {
		return err
	}

	return nil
}

func(labelsSql LabelsSQL) GetAllTestLabels() ([]*project.TestLabel, error) {
	var testLabels []*project.TestLabel

	err := labelsSql.engine.UseBool().Find(&testLabels)
	if err != nil {
		return nil, err
	}

	return testLabels, nil
}



