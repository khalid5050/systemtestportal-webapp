// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package dummydata

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// Users contains the dummy users for
// the default configuration of the system
var Users = []user.PasswordUser{
	{
		User: user.User{
			Name:             "default",
			DisplayName:      "DeFault",
			Email:            "default@example.org",
			RegistrationDate: time.Now().UTC().Round(time.Second),
			IsAdmin:          false,
			Biography:        "",
			IsShownPublic:    true,
			IsEmailPublic:    false,
			IsDeactivated:    false,
		},

		Password: "default",
	},
	{
		User: user.User{
			Name:             "admin",
			DisplayName:      "admin",
			Email:            "admin@example.org",
			RegistrationDate: time.Now().Round(time.Second),
			IsAdmin:          true,
			Biography:        "",
			IsShownPublic:    true,
			IsEmailPublic:    false,
			IsDeactivated:    false,
		},

		Password: "admin",
	},
	{
		User: user.User{
			Name:             "alexanderkaiser",
			DisplayName:      "Alexander Kaiser",
			Email:            "alexander.kaiser@gmx.de",
			RegistrationDate: time.Now().Round(time.Second),
			IsAdmin:          false,
			Biography:        "",
			IsShownPublic:    true,
			IsEmailPublic:    false,
			IsDeactivated:    false,
		},

		Password: "alexander",
	},
	{
		User: user.User{
			Name:             "simoneraab",
			DisplayName:      "Simone Raab",
			Email:            "simone.raab@gmail.com",
			RegistrationDate: time.Now().Round(time.Second),
			IsAdmin:          false,
			Biography:        "",
			IsShownPublic:    true,
			IsEmailPublic:    false,
			IsDeactivated:    false,
		},

		Password: "simone",
	},
	{
		User: user.User{
			Name:             "benweiss",
			DisplayName:      "Ben Weiss",
			Email:            "ben.weiss@yahoo.de",
			RegistrationDate: time.Now().Round(time.Second),
			IsAdmin:          false,
			Biography:        "",
			IsShownPublic:    true,
			IsEmailPublic:    false,
			IsDeactivated:    false,
		},

		Password: "ben",
	},
}
