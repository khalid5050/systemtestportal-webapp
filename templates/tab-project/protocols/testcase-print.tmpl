{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div id="tabTestCases">
    <div class="row tab-side-bar-row">
        <div class="col-md-12">
            <h4 class="mb-3">
                <span id="contentTestCaseResult" class="text-muted"></span>
                {{ .Project.Owner }}/{{ .Project.Name }} : <span id="protocolName">{{T "Protocol" .}}</span></h4>
            <div id="contentTestCaseNotesContainer" class="form-group d-none">
                <label><strong>{{T "Notes" .}}</strong></label>
                <p id="contentTestCaseNotes" class="text-muted">
                    {{T "No Notes" .}}
                </p>
            </div>
            <div class="form-group">
                <label><strong>{{T "Test Case Description" .}}</strong></label>
                <span id="contentTestCaseDescription" class="text-muted">
                    {{T "No Description" .}}
                </span>
            </div>
            <div class="form-group">
                <label><strong>{{T "Test Case Preconditions" .}}</strong></label>
                <p id="contentTestCasePreconditions" class="text-muted">
                    {{T "No Conditions" .}}
                </p>
            </div>
            <div class="form-group">
                <label><strong>{{T "Test Step Results" .}}</strong></label>
                <ul class="list-group" id="testStepsResultAccordion">
                </ul>
            </div>
            <div class="form-group">
                <label><strong>{{T "Execution Date" .}}</strong></label>
                <p id="contentTestCaseExecutionDate" class="text-muted">

                </p>
            </div>
            <div class="form-group">
                <label><strong>{{T "Tester" .}}</strong></label>
                <p id="contentTestCaseTester" class="text-muted">
                    {{T "some user" .}}
                </p>
            </div>
            <div class="form-group">
                <label><strong>{{T "System Version" .}}</strong></label>
                <p id="contentTestCaseSUTVersion" class="text-muted">
                    {{T "some system version" .}}
                </p>
            </div>
            <div class="form-group">
                <label><strong>{{T "System Variant" .}}</strong></label>
                <p id="contentTestCaseSUTVariant" class="text-muted">
                    {{T "some variant" .}}
                </p>
            </div>
            <div class="form-group">
                <label><strong>{{T "Test Case Version" .}}</strong></label>
                <p id="contentTestCaseVersion" class="text-muted">
                    {{T "some version" .}}
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/testprotocols.js" integrity="{{sha256 "/static/js/project/testprotocols.js"}}"></script>
{{ end }}