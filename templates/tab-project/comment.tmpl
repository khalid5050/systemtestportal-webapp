{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{/*

Usage information:

 - This template renders a list item as comment (so this should be used in a <ul></ul> environment)
 - The struct defining the input is located at "domain/comment/comment.go"

*/}}

{{define "comment" }}
    <li class="media" id="list-item-comment-{{ .Comment.Id }}">

        <!-- Image -->
            {{ if not (eq .Comment.Author.Name "") }}
            <a href="/users/{{.Comment.Author.Name}}" class="pull-left mr-3">
            {{ else }}
            <a href="#" class="pull-left mr-3">
            {{ end }}

            {{if not (eq .Comment.Author.Image "")}}
                <img src="{{getImagePath .Comment.Author.Image "user"}}" alt="" class="rounded-circle profile-picture" height="42" width="42">
            {{else}}
                <img src="/static/img/profile-placeholder.png" alt="" class="rounded-circle profile-picture" height="42" width="42">
            {{ end }}
            </a>


        <!-- Content -->
        <div class="media-body w-100">

            <!-- Top right -->
            <span class="text-muted pull-right align-middle">

                {{ if .Comment.IsEdit }}
                    <span class="small"> (edited) ·</span>
                {{ end }}

                <span data-toggle="tooltip" data-placement="top" title="{{ .Comment.UpdatedAt }}">
                    <time class="timeago text-muted small align-middle" datetime="{{ provideTimeago .Comment.UpdatedAt }}"></time>
                </span>

                {{ if .Comment.Requester }}
                    {{ if eq .Comment.Author.Name .Comment.Requester.Name}}
                        <span>·</span>

                        <button id="commentStartEditButton-{{ .Id }}"
                                type="button"
                                class="p-0 btn text-muted btn-link"
                                onclick="startCommentEditMode({{ .Id }})">
                            <i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit"></i>
                        </button>

                    {{ end }}
                {{ end }}

            </span>

            <!-- Top left -->
            {{ if and .Comment.Author.IsDeactivated .SystemSettings.IsDeleteUsers }} 
            <strong class=""> {{T "Anonymous" .}} </strong>
            {{ else }} 
            <strong class=""> {{ if not (eq .Comment.Author.Name "") }} {{ .Comment.Author.DisplayName }} {{ else }} deleted {{end}}</strong>
            {{ end }}
            {{ if .Comment.AuthorRole }} 
            <span class="text-muted">· {{ .Comment.AuthorRole.Name }}</span>
            {{ else }}
            <span class="text-muted">· non-member</span>
            {{ end }}
            <!-- Comment -->
            <div id="commentTextZone-{{ .Comment.Id }}"> {{ printMarkdown .Comment.Text }} </div>

            <!-- Comment Edit -->
            <div id="commentEditZone-{{ .Comment.Id }}" class="d-none">
                <div class="form-group">
                    <div class="md-area">

                        <!-- Keep unchanged text in case of abort-->
                        <p id="comment-original-{{ .Comment.Id }}" class="d-none">{{ .Comment.Text }}</p>
                        <textarea class="form-control markdown-textarea" style="resize: none;" id="inputCommentField-{{ .Comment.Id }}"
                                  oninput="onEditedCommentTextChanged({{ .Comment.Id }})"
                                  placeholder="{{T "Leave a comment" .}}" type="text" rows="3" maxlength="1000"
                                  aria-describedby="commentAreaDescriptor">{{ .Comment.Text }}</textarea>
                        <div class="md-area-bottom-toolbar" style="margin-bottom: 15px">
                            <div style="float:left">{{T "Markdown supported" .}}</div>

                            <button type="button" class="btn btn-success ml-2 float-right" id="submitCommentEditedButton-{{ .Comment.Id }}"
                                onclick="saveCommentEdited(this, {{ .Comment.Id }})" data-toggle="tooltip">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Save" .}} </span>
                            </button>

                            <button type="button" class="btn btn-danger mr-2 float-right"
                                onclick="abortCommentEditMode({{ .Comment.Id }})">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Cancel" .}} </span>
                            </button>
                        </div>
                    </div>
                    <div class="mb-2 row">
                        <div class="col-9">
                            <small class="text-muted">
                                <i>{{T "You have" .}} <span id="editCommentCharacterLeft-{{ .Comment.Id }}">1000</span> {{T "characters left" .}}.</i>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>

{{end}}