{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "content"}}
{{template "modal-generic-error" .}}

<div class="card p-5">
    <form id="newProjectForm" action="save">
        <div id="modalPlaceholder"></div>
        <div class="form-group">
            <div class="col-sm-10">
                <label for="inputProjectName">{{T "Project name" .}}</label>
                <input type="text" class="form-control" id="inputProjectName" name="inputProjectName"
                       aria-describedby="projectNameHelp" placeholder="{{T "Enter project name" .}}">
                <small id="projectNameHelp" class="form-text text-muted">
                    {{ T "This name is used wherever the project is mentioned." .}}
                </small>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10">
                <label for="inputProjectDesc">{{T "Project description" .}}</label>
                <div class="md-area">
                    <textarea class="form-control markdown-textarea" id="inputProjectDesc" name="inputProjectDesc" aria-describedby="projectDescHelp"
                              rows="3" placeholder="{{T "Describe your project in a few words" .}}."></textarea>
                    <div class="md-area-bottom-toolbar">
                        <div style="float:left">{{T "Markdown supported" .}}</div>
                    </div>
                </div>
                <small id="projectDescHelp" class="form-text text-muted">
                    {{ T "This description is used in the explore section." .}}
                </small>
            </div>
        </div>
        <div class="form-group" data-toggle="tooltip" data-placement="left"
             title="">
            <div class="col-sm-10">
                <label for="inputProjectLogo">{{ T "Project Logo" .}}</label>
                <div class="row tab-side-bar-row">
                    <div>

                        <image id="previewProjectLogo" src="/static/img/placeholder.svg" alt="project image" class="rounded" height="200" width="200"></image>

                    </div>
                    <div class="col-md-7 d-flex">
                        <div class="d-none d-sm-block tab-side-bar mr-3"></div>
                        <label class="btn btn-secondary btn-file mt-auto">{{T "Upload file" .}}
                            <input style="display: none;" class="" type="file" id="inputProjectLogo" name="inputProjectLogo" accept=".png,.jpg,.jpeg">
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <label>{{ T "Visibility" .}}</label>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="optionsProjectVisibility" id="optionPublic"
                       value="public" checked>
                <b>{{T "public" .}}</b> &mdash; {{ T "public projects are visible to everyone" .}}
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="optionsProjectVisibility"
                       id="optionInternal"
                       value="internal">
                <b>{{T "internal" .}}</b> &mdash; {{ T "internal projects are visible to all signed in users" .}}
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="optionsProjectVisibility" id="optionPrivate"
                       value="private">
                <b>{{T "private" .}}</b> &mdash; {{ T "private projects are only visible to members of these projects" .}}
            </label>
        </div>

    </form>


    <div class="form-group">
        <label for="importProject">{{ T "Import Project" .}}</label>
        <input type="file" class="form-control-file" id="importProject" name="importProject" accept="application/json">

    </div>
    <div>
        <button id="buttonCreateProject" type="submit" form="newProjectForm" class="btn btn-primary" disabled>{{ T "Create Project" .}}</button>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/creation/new-project.js" integrity="{{sha256 "/static/js/creation/new-project.js"}}"></script>
<script src="/static/assets/js/vendor/jquery.validate.min.js" integrity="{{sha256 "/static/assets/js/vendor/jquery.validate.min.js"}}"></script>
<script>
    var projects = {{.Projects}};
    initializeProjectCreateListener();
</script>
{{end}}


