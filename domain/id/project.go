/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

// ProjectExistenceChecker provides everything needed to check, if a project is already existent in storage.
type ProjectExistenceChecker interface {
	//Exists checks, if the given project is already existent in storage.
	Exists(id ProjectID) (bool, error)
}

//ProjectID hold all needed data to identify the a project in the whole system.
//This is the name and the owner of the project
type ProjectID struct {
	ActorID
	project string
}

//NewProjectID returns a new ProjectID containing the given name and owner. No validation will be performed.
func NewProjectID(owner ActorID, project string) ProjectID {
	return ProjectID{owner, project}
}

//Owner returns the name of the owner
func (id ProjectID) Owner() string {
	return id.Actor()
}

//Project returns the name of the (superior) project
func (id ProjectID) Project() string {
	return id.project
}

//Validate checks if this is a valid id for a new project.
func (id ProjectID) Validate(pec ProjectExistenceChecker) error {
	exists, err := pec.Exists(id)
	if err != nil {
		return err
	}
	if exists {
		return alreadyExistsErr(id)
	}
	return validateName(id.Project())
}
