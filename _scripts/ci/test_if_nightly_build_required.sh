CURRENT_DATE=`date +%s`
COMMIT_DATE=$(git show -s --format=%ct --date=short $(git rev-parse HEAD))

ONE_DAY_IN_SECONDS=86400

# "Current Unix Timestamp is: " $CURRENT_DATE
# "The Unix Timestamp of the last commit is: " $COMMIT_DATE

diff=$(($CURRENT_DATE - $COMMIT_DATE))
if [ $diff -gt $ONE_DAY_IN_SECONDS ]; then
    # "------------------ABORTING------------------------------------------------"
    # "Project will not be build again, because no new changes where added today!"
    echo "abort"
else
    # "------------------STARTING New Build--------------------------------------"
    # "New changes have been comitted in the last 24 hours, we will build a new nightly release." 
    # "Exit 0"
    echo "build"
fi