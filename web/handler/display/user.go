/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/contextdomain"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// RegisterUserGet is the handler that displays the registration site.
func RegisterUserGet(w http.ResponseWriter, r *http.Request) {
	contextEntities := handler.GetContextEntities(r)

	// If registration is not allowed and the user is not signed in return unauthorized request
	if !contextdomain.GetGlobalSystemSettings().IsRegistrationAllowed && contextEntities.User == nil {
		errors.Handle(handler.UnauthorizedAccess(r), w , r)
		return
	}

	// If the user is singed in but not an admin, return unauthorized request
	if contextEntities.User != nil {
		if !contextEntities.User.IsAdmin {
			errors.Handle(handler.UnauthorizedAccess(r), w , r)
			return
		}
	}

	tmpl := getNewUserTree(r)
	handler.PrintTmpl(context.New().WithUserInformation(r), tmpl, w, r)
}

// getNewUserTree returns the register template with all parent templates
func getNewUserTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// New user tree
		Append(templates.NewAccount).
		Get().Lookup(templates.HeaderDef)
}
