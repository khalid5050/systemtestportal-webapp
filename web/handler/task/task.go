/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package task

import (
	"encoding/json"
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/task"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

const (
	noSignedInUser    = "No signed-in user"
	noSignedInUserMsg = "You have to be signed-in to see your Tasks"
)

const (
	errCouldNotDecodeTaskItems    = "Could not update asks"
	errCouldNotDecodeTaskItemsMsg = "We were unable to decode the change to the tasks " +
		"send in your request. This ist most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."
)

// GetTask displays the list with the tasks of the signed-in user.
// task capsulate projectStore to projectGetter
func GetTask(taskListGetter handler.TaskListGetter, viewOpened bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		taskList, err := getTaskList(r, taskListGetter)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		templ := getTaskTree(viewOpened, r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.TaskList, taskList).
			With(context.TaskViewOpenedIssues, viewOpened),
			templ, w, r)
	}
}

func GetAsyncTaskTab(taskListGetter handler.TaskListGetter, viewOpened bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		taskList, err := getTaskList(r, taskListGetter)
		if err != nil {
			errors.Handle(err, w, r)
		}

		templ := handler.GetBaseTree(r).Append(getTabOpenedOrClosed(viewOpened)).Get().Lookup(templates.TaskTab)
		handler.PrintTmpl(context.New().WithUserInformation(r).With(context.TaskList, taskList), templ, w, r)
	}
}

// ItemPut sets a task item as done
func ItemPut(taskListGetter handler.TaskListGetter, taskListAdder handler.TaskListAdder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		contextEntities := handler.GetContextEntities(r)
		if contextEntities.User == nil {
			errors.Handle(
				errors.ConstructStd(http.StatusBadRequest, noSignedInUser, noSignedInUserMsg, r).
					WithLog("cant update tasks because no user is signed-in").
					WithStackTrace(1).
					Finish(), w, r)
			return
		}

		var itemIndex int
		if err := json.NewDecoder(r.Body).Decode(&itemIndex); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeTaskItems, errCouldNotDecodeTaskItemsMsg, r).
				WithLog("Couldn't read task items from request.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		taskList, err := taskListGetter.Get(contextEntities.User.Name)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		for i, item := range taskList.Tasks {
			if item.Index == itemIndex {
				taskList.Tasks[i].Done = !taskList.Tasks[i].Done
				break
			}
		}

		// Save updated list
		if err = taskListAdder.Add(*taskList); err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}

func getTaskList(r *http.Request, taskListGetter handler.TaskListGetter) (*task.List, error) {
	contextEntities := handler.GetContextEntities(r)
	if contextEntities.User == nil {
		return nil, errors.ConstructStd(http.StatusBadRequest, noSignedInUser, noSignedInUserMsg, r).
			WithLog("task list not available because no user is signed-in").
			WithStackTrace(1).
			Finish()
	}
	taskList, err := taskListGetter.Get(contextEntities.User.Name)
	if err != nil {
		return nil, err
	}
	return taskList, nil
}

// getTaskTree returns the template for the Tasks of a user
func getTaskTree(viewOpened bool, r *http.Request) *template.Template {

	return handler.GetNoSideBarTree(r).
		Append(templates.Task).
		Append(getTabOpenedOrClosed(viewOpened)).
		Get().Lookup(templates.HeaderDef)
}

func getTabOpenedOrClosed(viewOpened bool) string {
	if viewOpened {
		return templates.TaskOpened
	}
	return templates.TaskClosed
}
