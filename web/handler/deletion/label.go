/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package deletion

import (
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"net/http"
	"strconv"
)

/*
Deletes a Label for the given project
 */
func DeleteProjectLabelPut(labelStore handler.Labels) http.HandlerFunc{
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)

		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if c.User == nil || !c.Project.GetPermissions(c.User).EditProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		// Read in the data from the request
		labelId, err := strconv.ParseInt(r.FormValue(httputil.LabelId), 10, 64)
		if err != nil {
			errors.Handle(err, w, r)
		}

		err = labelStore.DeleteLabel(labelId)
		if err != nil {
			errors.Handle(err, w, r)
		}
	}
}

/*
Deletes a Testlabel for the given test and label
 */
func DeleteTestLabelPut(labelStore handler.Labels) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)

		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		// Read in the data from the request
		labelId, err := strconv.ParseInt(r.FormValue(httputil.LabelId), 10, 64)
		if err != nil {
			errors.Handle(err, w, r)
		}

		// Create a new testLabel based on case or sequence
		if c.Case == nil {

			// Check for sequence permissions
			if c.User == nil || !c.Project.GetPermissions(c.User).EditSequence {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			}

			err := labelStore.DeleteTestLabel(labelId, c.Sequence.Id, c.Project.Id, false)
			if err != nil {
				errors.Handle(err, w, r)
			}

		} else {

			// Check for case permissions
			if c.User == nil || !c.Project.GetPermissions(c.User).EditCase {
				errors.Handle(handler.UnauthorizedAccess(r), w, r)
				return
			}

			err := labelStore.DeleteTestLabel(labelId, c.Case.Id, c.Project.Id, true)
			if err != nil {
				errors.Handle(err, w, r)
			}

		}
	}
}